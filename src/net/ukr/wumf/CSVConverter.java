package net.ukr.wumf;

public interface CSVConverter {

	public String toCSVString();

	public Student fromCSVString(String str);

}
