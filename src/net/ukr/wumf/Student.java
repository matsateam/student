package net.ukr.wumf;

public class Student extends Human implements CSVConverter {
	private String groupName;
	private String id;

	public Student(String name, String lastName, int age, String groupName, String id) {
		super(name, lastName, age);
		this.groupName = groupName;
		this.id = id;
	}

	public Student() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toCSVString() {
		return getName() + " " + getLastName() + " " + getAge() + " " + getGroupName() + " " + getId();
	}

	@Override
	public Student fromCSVString(String str) {
		Student st = new Student();
		String[] str1 = str.split(" ");
		st.setName(str1[0]);
		st.setLastName(str1[1]);
		st.setAge(Integer.parseInt(str1[2]));
		st.setGroupName(str1[3]);
		st.setId(str1[4]);
		return st;
	}

	@Override
	public String toString() {
		return super.toString() + ", ������� " + groupName + ", id = " + id;
	}

}