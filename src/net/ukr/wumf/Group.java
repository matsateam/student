package net.ukr.wumf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

public class Group implements GroupDAO, CSVConverter {
	private String nameGroup;
	private Student[] array = new Student[10];

	public Group(String nameGroup) {
		super();
		this.nameGroup = nameGroup;
	}

	public Group() {
		super();
	}

	public String getNameGroup() {
		return nameGroup;
	}

	public void setNameGroup(String nameGroup) {
		this.nameGroup = nameGroup;
	}

	public Student[] getArray() {
		return array;
	}

	public void setArray(Student[] array) {
		this.array = array;
	}

	@Override
	public void saveGroup(File file) {
		StringBuilder stB = new StringBuilder();

		for (int i = 0; i < array.length; i++) {
			stB.append(array[i].toCSVString());
			stB.append("\n");
		}
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			out.println(stB);
			out.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	@Override
	public void loadGroup(File file, Group newGroup) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			for (; (line = br.readLine()) != null;) {
				String[] str1 = line.split(" ");
				if (str1.length == 1) {
					return;
				} else {
					Student st = new Student();
					st.setName(str1[0]);
					st.setLastName(str1[1]);
					st.setAge(Integer.parseInt(str1[2]));
					st.setGroupName(newGroup.getNameGroup());
					st.setId(str1[4]);
					try {
						newGroup.addStudent(st);

					} catch (GroupIsFullException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void addStudent(Student st) throws GroupIsFullException {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				array[i] = st;
				array[i].setGroupName(nameGroup);
				break;
			}
			if (i == array.length - 1) {
				throw new GroupIsFullException("  ");
			}
		}
	}

	public void deleteStudent(String id) {
		boolean check = true;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (id.equals(array[i].getId())) {
					array[i] = null;
					for (int k = i; k < array.length - 1; k++) {
						Student replace = array[k];
						array[k] = array[k + 1];
						array[k + 1] = replace;
					}
					check = false;
					break;
				}
			}
		}
		System.out.println((check) ? "    " : " c id " + id + " ");
	}

	public Student findStudent(String lastName) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (lastName.equals(array[i].getLastName())) {
					return array[i];
				}
			}
		}
		return null;
	}

	public void sortStudentsByLastName(Student[] students) {
		Arrays.sort(students, Comparator.nullsFirst(new StudentLastNameComparator()));
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		sortStudentsByLastName(array);

		stringBuilder.append("\n");
		stringBuilder.append(" " + nameGroup + " : ");
		stringBuilder.append("\n");
		stringBuilder.append("\n");
		for (Student s : array) {
			if (s != null) {
				stringBuilder.append(s.getLastName() + " " + s.getName() + ", : " + s.getAge() + ";")
						.append("\n");
			}
		}
		stringBuilder.append("\n");

		return stringBuilder.toString();
	}

	@Override
	public String toCSVString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student fromCSVString(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
