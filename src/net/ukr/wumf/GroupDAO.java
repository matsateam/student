package net.ukr.wumf;

import java.io.File;

public interface GroupDAO {
	
	public void saveGroup(File file);

	public void loadGroup(File file, Group newGroup);

}
