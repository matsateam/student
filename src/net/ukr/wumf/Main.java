package net.ukr.wumf;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {
		
		try {
			Files.delete(Paths.get("group1.txt"));
			Files.delete(Paths.get("group2.txt"));
		} catch (IOException x) {
			System.err.println(x);
		}
		
		Student stud1 = new Student("Мыкола", "Тульский", 25, "G10", "1");
		Student stud2 = new Student("Вася", "Плотык", 22, "G10", "2");
		Student stud3 = new Student("Сергей", "Мосейчук", 23, "G10", "3");
		Student stud4 = new Student("Пэдро", "Родригес", 22, "G10", "4");
		Student stud5 = new Student("Иван", "Татарин", 24, "G10", "5");
		Student stud6 = new Student("Юля", "Ветлицкая", 21, "G10", "6");
		Student stud7 = new Student("Филипп", "Киркоров", 27, "G10", "7");
		Student stud8 = new Student("Назар", "Квазар", 23, "G10", "8");
		Student stud9 = new Student("Ира", "Ковш", 22, "G10", "9");
		Student stud10 = new Student("Лиза", "Пинтковская", 21, "G10", "10");

		Group g10 = new Group("G10");
		Group g11 = new Group("G11");
		
		try {
			g10.addStudent(stud1);
			g10.addStudent(stud2);
			g10.addStudent(stud3);
			g10.addStudent(stud4);
			g10.addStudent(stud5);
			g10.addStudent(stud6);
			g10.addStudent(stud7);
			g10.addStudent(stud8);
			g10.addStudent(stud9);
			g10.addStudent(stud10);

		} catch (GroupIsFullException e) {
			e.printStackTrace();
		}

		File group1 = new File("group1.txt");
		File group2 = new File("group2.txt");
		
		g10.saveGroup(group1);

		g11.loadGroup(group1, g11);

		g11.saveGroup(group2);
		
		System.out.println(g10);
		System.out.println(g11);
		

	}
}
